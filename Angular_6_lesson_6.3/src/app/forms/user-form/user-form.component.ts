import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  public user: User;

  public formSubmited: boolean;

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.formSubmited = false;
    this.user = {
      id: 1,
      firstName: "Kot",
      lastName: "Matroskin"
    } 
  }
  
  public onSubmited()
  {
    // todo: реализовать логику по отправке данных
    this.formSubmited = true;
  }

  public cleanButtonClicked()
  {    
    this.user = new User();   
  }
}
