import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
      {id: 1, firstName: 'Ivan', lastName: 'Ivanov'},
      {id: 2, firstName: 'Petr', lastName: 'Petrov'},
      {id: 3, firstName: 'Vasya', lastName: 'Hmuriy'},  
      {id: 4, firstName: 'Vasya', lastName: 'Hmuriy'},  
      {id: 5, firstName: 'Vasya', lastName: 'Hmuriy'},
      {id: 6, firstName: 'Fedya', lastName: 'Fedorov'},
      {id: 7, firstName: 'Sasha', lastName: 'Grey'},
      {id: 8, firstName: 'Gosha', lastName: 'Alphabet'},
      {id: 9, firstName: 'Uchi', lastName: 'Angular'},
      {id: 10, firstName: 'Eto', lastName: 'Kruto'},
    ];

    return { users }; 
  }

  constructor() { }
}
