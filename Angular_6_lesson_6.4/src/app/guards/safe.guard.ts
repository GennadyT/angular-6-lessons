import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CanDeactivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SafeGuard implements CanDeactivate<> {
 
}
