import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UsersService } from '../../services/users.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  public user: User = new User();

  public formSubmited: boolean;

  @ViewChild('userForm')
  private userForm: NgForm;
  
  constructor(
    private userService: UsersService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.formSubmited = false;

    let id = this.route.snapshot.paramMap.get('id');
    this.userService.getUser(+id)
      .subscribe(u => {
        this.user = u;
      })
  }

  public hasUnsavedData() : boolean {
    return this.userForm.dirty;
  }
  
  public onSubmited()
  {
    // todo: реализовать логику по отправке данных
    this.formSubmited = true;
  }
}
